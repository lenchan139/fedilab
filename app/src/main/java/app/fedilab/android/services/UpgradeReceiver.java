package app.fedilab.android.services;
/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;



public class UpgradeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction() != null && intent.getAction().compareTo(Intent.ACTION_MY_PACKAGE_REPLACED) == 0) {
            Intent streamingServiceIntent = new Intent(context, LiveNotificationDelayedService.class);
            streamingServiceIntent.putExtra("stop", true);
            try {
                context.startService(streamingServiceIntent);
            } catch (Exception ignored) {
            }
            streamingServiceIntent = new Intent(context, LiveNotificationService.class);
            streamingServiceIntent.putExtra("stop", true);
            try {
                context.startService(streamingServiceIntent);
            } catch (Exception ignored) {
            }
        }
    }
}