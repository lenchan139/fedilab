package app.fedilab.android.drawers;
/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */

import android.content.Intent;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;

import com.vanniktech.emoji.EmojiManager;
import com.vanniktech.emoji.EmojiPopup;
import com.vanniktech.emoji.one.EmojiOneProvider;

import app.fedilab.android.activities.OwnerNotificationChartsActivity;
import app.fedilab.android.asynctasks.PostActionAsyncTask;
import app.fedilab.android.asynctasks.RetrieveFeedsAsyncTask;
import app.fedilab.android.client.API;
import app.fedilab.android.client.Entities.Reaction;
import app.fedilab.android.client.Entities.Status;
import app.fedilab.android.client.Entities.StatusDrawerParams;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class StatusListAdapter extends BaseStatusListAdapter {
    public StatusListAdapter(StatusDrawerParams statusDrawerParams) {
        super(statusDrawerParams);
    }

    public void statusAddReactionClick(Status status, ViewHolder holder) {
        EmojiManager.install(new EmojiOneProvider());
        final EmojiPopup emojiPopup = EmojiPopup.Builder.fromRootView(holder.status_add_reaction).setOnEmojiPopupDismissListener(() -> {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(holder.status_add_reaction.getWindowToken(), 0);
        }).setOnEmojiClickListener((emoji, imageView) -> {
            String emojiStr = imageView.getUnicode();
            boolean alreadyAdded = false;
            for (Reaction reaction : status.getReactions()) {
                if (reaction.getName().compareTo(emojiStr) == 0) {
                    alreadyAdded = true;
                    reaction.setCount(reaction.getCount() - 1);
                    if (reaction.getCount() == 0) {
                        status.getReactions().remove(reaction);
                    }
                    notifyStatusChanged(status);
                    break;
                }
            }
            if (!alreadyAdded) {
                Reaction reaction = new Reaction();
                reaction.setMe(true);
                reaction.setCount(1);
                reaction.setName(emojiStr);
                status.getReactions().add(0, reaction);
                notifyStatusChanged(status);
            }
            API.StatusAction statusAction;
            if (type == RetrieveFeedsAsyncTask.Type.ANNOUNCEMENTS) {
                statusAction = alreadyAdded ? API.StatusAction.REMOVE_REACTION : API.StatusAction.ADD_REACTION;
            } else {
                statusAction = alreadyAdded ? API.StatusAction.REMOVE_PLEROMA_REACTION : API.StatusAction.ADD_PLEROMA_REACTION;
            }
            new PostActionAsyncTask(context, statusAction, status.getId(), null, emojiStr, StatusListAdapter.this).execute();
        })
                .build(holder.fake_edittext);
        emojiPopup.toggle();
    }

    protected void notificationCharts(Status status) {
        Intent intent = new Intent(context, OwnerNotificationChartsActivity.class);
        Bundle b = new Bundle();
        b.putString("status_id", status.getReblog() != null ? status.getReblog().getId() : status.getId());
        intent.putExtras(b);
        context.startActivity(intent);
    }

}
