Fixed:
- Issue with not clickable URLs
- Custom emoji in polls not displayed
- Auto-split toots feature