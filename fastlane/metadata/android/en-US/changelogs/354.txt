Added:
- Announcements for Mastodon with reactions
- Pleroma: reactions in posts

Changed:
- Filters changed to speed-up the scroll

Fixed:
- Custom emojis not displayed in compose activity