Changed:
- Improve memory management
- Improve scroll
- One logout entry in the menu (it will remove the account from the app)
- Clear push notifications when visiting notifications tab

Fixed:
- Long press to store media download the preview image
- Fix pagination with Nitter timelines